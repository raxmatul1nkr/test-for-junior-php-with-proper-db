<?php
$output .= "  
<table class='table table-bordered  border-primary'>
<thead>
  <tr>
    <th scope='col'>Имя</th>
    <th scope='col'>Фамилия</th>
    <th scope='col'>Дата увольнения</th>
    <th scope='col'>Причина увольнения</th>
  </tr>
</thead> 
<tbody>
";  
while($row = mysqli_fetch_array($result))  
{  
     $output .= '  
          <tr>  
               <td>'.$row["first_name"].'</td> 
               <td>'.$row["last_name"].'</td>  
               <td>'.$row["update_at"].'</td>
               <td>'.$row["description"].'</td>    
          </tr>  
     ';  
}  