<?php
$output .= "  
<table class='table table-bordered  border-primary'>
<thead>
  <tr>
    <th scope='col'>Имя</th>
    <th scope='col'>Фамилия</th>
    <th scope='col'>Отчество</th>
    <th scope='col'>Дата рождения</th>
    <th scope='col'>Дата устройства на работу</th>
  </tr>
</thead> 
<tbody>
";  
while($row = mysqli_fetch_array($result))  
{  
     $output .= '  
          <tr>  
               <td>'.$row["first_name"].'</td> 
               <td>'.$row["last_name"].'</td> 
               <td>'.$row["middle_name"].'</td>  
               <td>'.$row["data_of_birth"].'</td>  
               <td>'.$row["created_at"].'</td>  
          </tr>  
     ';  
}  