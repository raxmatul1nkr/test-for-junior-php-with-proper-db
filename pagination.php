<?php  
 //pagination.php  
 mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

 try{
 $connect = mysqli_connect("db", "root", "1234", "qwerty");
 }catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
 }  
 $record_per_page = 10;  
 $page = '';  
 $output = '';  
 if(isset($_POST["page"]))  
 {  
      $page = $_POST["page"];  
 }  
 else  
 {  
      $page = 1;  
 }  
 $query = "";
 $start_from = ($page - 1)*$record_per_page;  

 $a = $_POST["method"];
 if($a == 2)  
 {  
     $query = "SELECT  last_name, first_name, middle_name, data_of_birth, created_at FROM qwerty.user WHERE CURDATE()<= date_add(created_at, INTERVAL 3 MONTH) ORDER BY last_name, first_name, middle_name LIMIT $start_from, $record_per_page;";  
 } else if ($a==1){
     $query = "SELECT * FROM qwerty.user LIMIT $start_from, $record_per_page;";  
 }else if($a==3){
     $query = "SELECT user.first_name , user.last_name , 
     user_dismission.update_at ,
     dismission_reason.description 
     FROM qwerty.user_dismission left join qwerty.user on  user_id=user.id left join qwerty.dismission_reason
     on qwerty.user_dismission.reason_id = qwerty.dismission_reason.id LIMIT $start_from, $record_per_page;";  
 }else if ($a==4){
     $query = "SELECT max(t.created_at) ,  
     user.last_name as gg,
     department.leader_id as li,
      (select b.last_name from qwerty.user_position as a left join qwerty.user as b on a.user_id = b.id 
      left join qwerty.department as d on department_id = d.id
      where max(t.created_at) = a.created_at and d.leader_id=department.leader_id order by a.created_at, a.user_id desc   limit 1) as ln,
      (select b.first_name from qwerty.user_position as a left join qwerty.user as b on a.user_id = b.id 
      left join qwerty.department as d on department_id = d.id
      where max(t.created_at) = a.created_at and d.leader_id=department.leader_id order by a.created_at, a.user_id desc   limit 1) as fn
    FROM qwerty.user_position as t  left join  qwerty.user as user1  on t.user_id = user1.id 
    left join qwerty.department on department_id = department.id
    left join qwerty.user  on leader_id = user.id group by leader_id LIMIT $start_from, $record_per_page;";  

 }
 try{
 $result = mysqli_query($connect, $query);  
 }catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
 }  
 if($a == 2) {
   include "view/probation.php";
 }
  else if ($a==1){
    include "view/all.php";
 }else if($a==3){
    include "view/dismissed.php";
 }else if ($a==4){
    include "view/head.php";
 }
 $output .= '</table><br /><div align="center">';   
 if($a == 2)  { 
     $page_query = "SELECT count(*) FROM qwerty.user WHERE CURDATE()<= date_add(created_at, INTERVAL 3 MONTH) ORDER BY last_name, first_name, middle_name;";
 } else if ($a == 1) {
     $page_query = "SELECT count(*) FROM qwerty.user;";  
 } else if($a == 3){
     $page_query = "SELECT count(*) FROM qwerty.user_dismission left join qwerty.user on  user_id=user.id left join qwerty.dismission_reason
     on qwerty.user_dismission.reason_id = qwerty.dismission_reason.id;"; 
 } else if($a == 4){
     $page_query = "SELECT count(*) FROM qwerty.user_position as t  left join  qwerty.user as user1  on t.user_id = user1.id 
     left join qwerty.department on department_id = department.id
     left join qwerty.user  on leader_id = user.id group by leader_id;"; 
 }
 $page_result = mysqli_query($connect, $page_query);  
 $total_records = $page_result->fetch_row()[0];
 $total_pages = ceil($total_records/$record_per_page);
 
 for($i=1; $i<=$total_pages; $i++)  
 {  
      $output .= "<span class='pagination_link' style='cursor:pointer; padding:6px; border:1px solid #ccc;' id='".$i."'>".$i."</span>";  
 }  
 $output .= '</div><br /><br />';  
 echo $output;  
 ?>  